import ReactDOM from 'react-dom'
import * as serviceWorkerRegistration from './serviceWorkerRegistration'
import { ProvideAuth } from './contexts/AuthContext'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { routes } from './configs/routes'
import { PrivateRoute } from './components/PrivateRoute'
import './assets/main.css'

const App = () => {

  const renderRoutes = () => {
    const routesArray = []
    routes.forEach(route => {
      if (!route.private) routesArray.push(<Route key={route.name} exact path={route.path} render={(props) => <route.component {...props} />} />)
      if (route.private) routesArray.push(<PrivateRoute key={route.name} exact path={route.path} component={route.component} />)
    })
    return routesArray
  }

  return (
    <BrowserRouter>
      <div className='h-screen flex flex-col bg-gray-50'>
        <div className="flex flex-col justify-center items-center py-auto my-auto text-gray-700">
          <Switch>
            {renderRoutes()}
            <Redirect from='/' to='/chat' />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  )
}

ReactDOM.render(<ProvideAuth><App /></ProvideAuth>, document.getElementById('root'))
serviceWorkerRegistration.register() 
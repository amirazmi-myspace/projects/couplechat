import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'

export const Signup = () => {

  const [errorMessage, setErrorMessage] = useState()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [passwordConfirm, setPasswordConfirm] = useState('')
  const { signup } = useAuth()
  const history = useHistory()

  useEffect(() => { setErrorMessage(false) }, [email, password, passwordConfirm])

  const submit = async (e) => {
    e.preventDefault()
    if (email === '') return setErrorMessage('Email must not empty!')
    if (password === '') return setErrorMessage('Password must not be empty!')
    if (passwordConfirm !== password) return setErrorMessage('The password must be the same!')
    if (!email.includes('@')) return setErrorMessage('Must be valid email!')
    try {
      const user = await signup(email, password)
      history.push('/new-user')
      history.push({ pathname: '/new-user', state: { user } })
    } catch (error) {
      setErrorMessage(error.message)
    }
  }

  const renderForm = () => (
    <form onSubmit={submit}>
      <div className="">
        <label htmlFor="email">Email</label>
        <input type="text" name='email' onChange={(e) => setEmail(e.target.value)} />
      </div>
      <div className="">
        <label htmlFor="password">Password</label>
        <input type="password" name='password' onChange={(e) => setPassword(e.target.value)} />
      </div>
      <div className="">
        <label htmlFor="confirmPassword">Confirm Password</label>
        <input type="password" name='confirmPassword' onChange={(e) => setPasswordConfirm(e.target.value)} />
      </div>
      <div className="">
        <button type='submit'>Sign Up</button>
      </div>
    </form>
  )

  return (
    <div className='w-4/5 sm:w-3/5 md:w-2/5 lg:w-2/6'>
      <div>
        <h2>Welcome to CoupleChat</h2>
        <small>Sign up and tell your partner to join private chat only with you.</small>
        {errorMessage && <div>{errorMessage}</div>}
      </div>
      {renderForm()}
    </div>
  )
}

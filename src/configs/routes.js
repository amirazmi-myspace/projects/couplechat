import { Chat } from "../screens/Chat";
import { NewUser } from "../screens/NewUser";
import { Profile } from "../screens/Profile";
import { Signin } from "../screens/Signin";
import { Signup } from "../screens/Signup";

export const routes = [
  { name: 'Sign Up', path: '/signup', component: Signup, private: false },
  { name: 'Sign In', path: '/signin', component: Signin, private: false },
  { name: 'Profile', path: '/profile', component: Profile, private: true },
  { name: 'Chat', path: '/chat', component: Chat, private: true },
  { name: 'New User', path: '/new-user', component: NewUser, private: true },
]